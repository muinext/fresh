<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    });
%>
<div class="col-md-12">
            <ul class="nav nav-pills nav-justified">
            <% if (strpos($action, 'add') === false): %>
            <li role="presentation"><?php echo $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $<%= $singularVar %>-><%= $primaryKey[0] %>],
            ['confirm' => __('Are you sure you want to delete # {0}?', $<%= $singularVar %>-><%= $primaryKey[0] %>)]
            )
            ?></li>
        <% endif; %>
        <li role="presentation"><?php echo $this->Html->link(__('List <%= $pluralHumanName %>'), ['action' => 'index']) ?></li>
        <%
        $done = [];
        foreach ($associations as $type => $data) {
        foreach ($data as $alias => $details) {
        if ($details['controller'] !== $this->name && !in_array($details['controller'], $done)) {
        %>
        <li role="presentation"><?php echo $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'index']) %></li>
            <li><?php echo $this->Html->link(__('New <%= $this->_singularHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'add']) %></li>
            <%
            $done[] = $details['controller'];
            }
            }
            }
            %>
            </ul>
<fieldset>
    <legend><strong><?php echo __('<%= Inflector::humanize($action) %> <%= $singularHumanName %>') ?></strong></legend>

    <?php echo $this->Form->create($<%= $singularVar %>,['role'=>'form','novalidate'=>'novalidate']) ?>

        <%
        foreach ($fields as $field) {
        if (in_array($field, $primaryKey)) {
        continue;
        }
        %>
    <div class="form-group">
    <?php
    <%
        if (isset($keyFields[$field])) {
        $fieldData = $schema->column($field);
        if (!empty($fieldData['null'])) {
        %>
        echo $this->Form->input('<%= $field %>', ['options' => $<%= $keyFields[$field] %>, 'empty' => true,'class'=>'form-control','placeholder'=>'Enter <%= Inflector::humanize($field) %>']);
        <%
        } else {
        %>
        echo $this->Form->input('<%= $field %>', ['options' => $<%= $keyFields[$field] %>,'class'=>'form-control','placeholder'=>'Enter <%= Inflector::humanize($field) %>']);
        <%
        }
    %>
    ?>
    </div>
    <%
        continue;
        }
        if (!in_array($field, ['created', 'modified', 'updated'])) {
        $fieldData = $schema->column($field);
        if (($fieldData['type'] === 'date') && (!empty($fieldData['null']))) {
        %>
        echo $this->Form->input('<%= $field %>', ['empty' => true, 'default' => '','class'=>'form-control','placeholder'=>'Enter <%= Inflector::humanize($field) %>']);
        <%
        } else {
        %>
        echo $this->Form->input('<%= $field %>',['class'=>'form-control','placeholder'=>'Enter <%= Inflector::humanize($field) %>']);
        <%
        }
        }
    %>
    ?>
    </div>
    <%
        }

        if (!empty($associations['BelongsToMany'])) {
        foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
    %>
    <?php
        echo $this->Form->input('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]);
    ?>
        <%
        }
        }
        %>
    <?php echo $this->Form->button(__('Submit'),['class'=>'btn btn-rounded btn-success btn-sm']) ?>
    <?php echo $this->Form->end() ?>

            </fieldset>
</div>
