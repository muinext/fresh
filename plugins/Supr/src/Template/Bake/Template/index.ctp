<%
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.1.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
use Cake\Utility\Inflector;

$fields = collection($fields)
->filter(function($field) use ($schema) {
return !in_array($schema->columnType($field), ['binary', 'text']);
})
->take(7);
%>
<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('New <%= $singularHumanName %>'), ['action' => 'add']) ?></li>
        <%
        $done = [];
        foreach ($associations as $type => $data):
        foreach ($data as $alias => $details):
        if (!empty($details['navLink']) && $details['controller'] !== $this->name && !in_array($details['controller'], $done)):
        %>
        <li role="presentation"><?php echo $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'index']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('New <%= $this->_singularHumanName($alias) %>'), ['controller' => '<%= $details['controller'] %>', 'action' => 'add']) ?></li>
        <%
        $done[] = $details['controller'];
        endif;
        endforeach;
        endforeach;
        %>
    </ul>
    <fieldset>
        <legend><strong><?php echo __('<%= $pluralHumanName %>') ?></strong></legend>
        <table class="table table-bordered table-striped table-hover table-custom dt-responsive responsive-datatables" width="100%">
            <thead>
            <tr>
                <% foreach ($fields as $field): %>
                <th><?php echo $this->Paginator->sort('<%= $field %>') ?></th>
                <% endforeach; %>
                <th class="actions"><?php echo __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($<%= $pluralVar %>) && $<%= $pluralVar %>): ?>
            <?php foreach ($<%= $pluralVar %> as $key => $<%= $singularVar %>): ?>
            <tr>
                <%        foreach ($fields as $field) {
                $isKey = false;
                if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                if ($field === $details['foreignKey']) {
                $isKey = true;
                %>
                <td><?php echo $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                <%
                break;
                }
                }
                }
                if ($isKey !== true) {
                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
                %>
                <td><?php echo h($<%= $singularVar %>-><%= $field %>) ?></td>
                <%
                } else {
                %>
                <td><?php echo $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
                <%
                }
                }
                }

                $pk = '$' . $singularVar . '->' . $primaryKey[0];
                %>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), ['action' => 'view', <%= $pk %>]) ?>
                    <?php echo $this->Html->link(__('Edit'), ['action' => 'edit', <%= $pk %>]) ?>
                    <?php echo $this->Form->postLink(__('Delete'), ['action' => 'delete', <%= $pk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </fieldset>
    <div class="paginator">
        <ul class="pagination">
            <?php echo $this->Paginator->prev('< ' . __('previous')) ?>
            <?php echo $this->Paginator->numbers() ?>
            <?php echo $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?php echo $this->Paginator->counter() ?></p>
    </div>
</div>
