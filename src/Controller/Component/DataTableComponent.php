<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Core\App;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Routing\Router;
use Cake\Utility\Hash;

/**
 * DataTable component
 */
class DataTableComponent extends Component
{
    use EventDispatcherTrait;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $components = ['RequestHandler', 'Flash'];

    private $model;
    private $controller;
    private $times = array();
    public $conditionsByValidate = 0;
    public $emptyElements = 0;
    public $fields = array();
    public $mDataProp = true;

    public function initialize(array $config)
    {
        $controller = $this->_registry->getController();
        $this->eventManager($controller->eventManager());
        $this->response =& $controller->response;
        $this->session = $controller->request->session();

        /* old codes */
        $this->controller = $controller;
        $modelName = $this->controller->modelClass;
        $this->model = $this->controller->{$modelName};
    }

    /**
     * returns dataTables compatible array - just json_encode the resulting aray
     * @param object $controller optional
     * @param object $model optional
     * @return array
     */
    public function getResponse($controller = null, $model = null)
    {

        /**
         * it is no longer necessary to pass in a controller or model
         * this is handled in the initialize method
         * $controller is disregarded.
         * $model is only necessary if you are using a model from a different controller such as if you are in
         * a CustomerController but your method is displaying data from an OrdersModel.
         */

        $this->setTimes('Pre', 'start', 'Preproccessing of conditions');

        if ($model != null) {
            if (is_string($model)) {
                $this->model = $this->controller->{$model};
            } else {
                $this->model = $model;
                unset($model);
            }
        }

        $conditions = isset($this->controller->paginate['conditions']) ? $this->controller->paginate['conditions'] : null;

        $isFiltered = false;

        if (!empty($conditions)) {
            $isFiltered = true;
        }

        if (isset($this->controller->request->query)) {
            $httpGet = $this->controller->request->query;
        }

        // check for ORDER BY in GET request
        if (isset($httpGet)) {
            $orderBy = $this->order($httpGet);
            if (!empty($orderBy)) {
                $this->controller->paginate = array_merge($this->controller->paginate, array('order' => $orderBy));
            }
        }

        // check for WHERE statement in GET request
        if (isset($httpGet) && !empty($httpGet['search'])) {
            $conditions = $this->filter($httpGet);

            if (!empty($conditions)) {
                $this->controller->paginate = array_merge_recursive($this->controller->paginate, array('conditions' => array('OR' => $conditions)));
                $isFiltered = true;
            }
        }

        $this->setTimes('Pre', 'stop');
        $this->setTimes('Count All', 'start', 'Counts all records in the table');
        // @todo avoid multiple queries for finding count, maybe look into "SQL CALC FOUND ROWS"
        // get full count
        $total = $this->model->find()->count();
        $this->setTimes('Count All', 'stop');
        $this->setTimes('Filtered Count', 'start', 'Counts records that match conditions');
        $parameters = $this->controller->paginate;

        if ($isFiltered) {
            $filteredTotal = $this->model->find('list', $parameters)->count();
        }
        $this->setTimes('Filtered Count', 'stop');
        $this->setTimes('Find', 'start', 'Cake Find');

        $draw = isset($this->controller->request->query['draw']) ? intval($this->controller->request->query['draw']) : 1;

        $page = 1;
        // set sql limits
        if (isset($this->controller->request->query['start']) && $this->controller->request->query['length'] != '-1') {
            $start = $this->controller->request->query['start'];
            $length = $this->controller->request->query['length'];
            $parameters['limit'] = $length;
            $parameters['offset'] = $start;
            $this->controller->paginate['limit'] = $length;

            $page = $start < $length ? 1 : ($start / $length) + 1;

        }

        $this->controller->paginate['maxLimit'] = '1000';
        $this->controller->paginate['page'] = $page;

        //console_log($httpGet);


        if ($this->controller->request->is(['ajax'])) {
            //pr($httpGet);
        }

        $query = $this->model->find();
        $query->hydrate(false);

        // execute sql select
        $data = $this->controller->paginate($query);

        $this->setTimes('Find', 'stop');
        $this->setTimes('Response', 'start', 'Formatting of response');
        // dataTables compatible array
        $response = array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $isFiltered === true ? $filteredTotal : $total,
            'data' => $data
        );

        $this->setTimes('Response', 'stop');
        return $response;
    }

    /**
     * setTimes method - adds to timer of settings[timed] = true
     * @param string $key
     * @param string $action (start or stop)
     * @param string $desc (optional)
     * @param string $line
     */
    private function setTimes($key, $action, $desc = '')
    {
        if (isset($this->settings) && isset($this->settings['timer']) && $this->settings['timer'] == true) {
            $this->times[$key][$action] = array(
                'action' => $action,
                'time' => microtime(true),
                'description' => $desc
            );
        }
    }

    /**
     * getTimes method - returns an array of the components benchmarks
     * @return type
     */
    public function getTimes()
    {
        $times = array();
        $componentStart = 0;
        foreach ($this->times as $x => $i) {
            $start = $end = $desc = $startLine = $endLine = 0;
            foreach ($i as $j) {
                if ($j['action'] == 'start') {
                    $start = $j['time'];
                    $desc = $j['description'];
                    if ($componentStart == 0) {
                        $componentStart = $start;
                    }
                } else if ($j['action'] == 'stop') {
                    $end = $j['time'];
                }
                if ($start > 0 && $end > 0) {
                    $times[$x] = array(
                        'description' => $desc,
                        'time' => round(($end - $start), 4),
                    );
                }
            }
        }

        if (isset($this->settings) && isset($this->settings['timer']) && $this->settings['timer'] == true) {
            $times['TOTAL'] = array(
                'time' => round(($end - $componentStart), 4)
            );
        }

        return $times;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @return string SQL order by clause
     */
    static function order($request)
    {
        $order = [];
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';
                    $order[$requestColumn['data']] = $dir;
                }
            }
        }
        return $order;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     *    sql_exec() function
     * @return string SQL where clause
     */
    static function filter($request)
    {
        $where = array();
        if (isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                if ($requestColumn['searchable'] == 'true') {
                    $where[$requestColumn['data'] . " LIKE "] = "%{$str}%";
                }
            }
        }
        return $where;
    }

}
