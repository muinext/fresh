<?php
echo $this->Html->script('/bowers/datatables.net/js/jquery.dataTables.min', ['block' => 'scriptBlock']);
echo $this->Html->script('/bowers/datatables.net-bs/js/dataTables.bootstrap.min', ['block' => 'scriptBlock']);
echo $this->Html->script('/bowers/datatables.net-responsive/js/dataTables.responsive.min', ['block' => 'scriptBlock']);
?>
<?php
if (!isset($actionsExclude) || !in_array('View', $actionsExclude)) {
    $actionsArr[] = $this->Html->link(__('View'), ['action' => 'view', 'itemId']);
}
if (!isset($actionsExclude) || !in_array('Edit', $actionsExclude)) {
    $actionsArr[] = $this->Html->link(__('Edit'), ['action' => 'edit', 'itemId']);
}
if (!isset($actionsExclude) || !in_array('Delete', $actionsExclude)) {
    $actionsArr[] = $this->Form->postLink(__('Delete'), ['action' => 'delete', 'itemId'], ['confirm' => __('Are you sure you want to delete # {0}?', 'itemId')]);
}
$actionsStr = implode('&nbsp;', ($actionsArr ? $actionsArr : []));
?>
<?php $this->Html->scriptStart(['block' => true]); ?>
<?php ob_start(); ?>
    <SCRIPT>
        $(document).ready(function () {
            if ($('.server-datatables').length > 0) {
                $('.server-datatables').each(function () {
                    var ajax_target = "<?php echo $this->request->here . '.json'; ?>";
                    $(this).dataTable({
                        //"pagingType": "full_numbers",
                        "info": true,
                        "order": [],
                        "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        }],
                        aLengthMenu: [
                            [25, 50, 100, 200, 500],
                            [25, 50, 100, 200, 500]
                        ],
                        "iDisplayLength": 25,
                        "processing": true,
                        "serverSide": true,
                        "ajax": ajax_target,
                        "columns": <?php echo isset($tableColumns) ? $tableColumns : json_encode([]); ?>,
                        "rowCallback": function (row, data, index) {
                            $('td:eq(0)', row).html(index + 1);
                            if (data.id) {
                                var actionThIndex = $('th.actions').index();
                                if (actionThIndex != -1) {
                                    var demoStr = '<?php echo $actionsStr; ?>';
                                    var finalStr = demoStr.replace(/itemId/gi, data.id);
                                    $('td:eq(' + actionThIndex + ')', row).html(finalStr);
                                }
                            }
                        },
                        "oLanguage": {
                            "sSearch": "",
                            "sLengthMenu": "<span>_MENU_</span>"
                        },
                        "sDom": "<'row'<'col-md-6 col-xs-12 p0 'l><'col-md-6 col-xs-12 p0'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>"
                    });
                });
                $('.table').checkAll({
                    masterCheckbox: '.check-all',
                    otherCheckboxes: '.check',
                    highlightElement: {
                        active: true,
                        elementClass: 'tr',
                        highlightClass: 'highlight'
                    }
                });
            }
        });
        //------------- tables-data.js -------------//
        $(document).ready(function () {

            //------------- Data tables -------------//
            //basic datatables
            if ($('.basic-datatables').length > 0) {
                $('.basic-datatables').each(function () {
                    $(this).dataTable({
                        "oLanguage": {
                            "sSearch": "",
                            "sLengthMenu": "<span>_MENU_</span>"
                        },
                        columnDefs: [
                            {orderable: false, targets: ['no-sort']}
                        ],
                        "sDom": "<'row'<'col-md-6 col-xs-12 p0 'l><'col-md-6 col-xs-12 p0'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>"
                    });
                });
            }

            //------------- Check all checkboxes -------------//
            $('.table').checkAll({
                masterCheckbox: '.check-all',
                otherCheckboxes: '.check',
                highlightElement: {
                    active: true,
                    elementClass: 'tr',
                    highlightClass: 'highlight'
                }
            });

            /*responsive datatables*/
            if ($('.responsive-datatables').length > 0) {
                $('.responsive-datatables').each(function () {
                    $(this).dataTable({
                        "oLanguage": {
                            "sSearch": "",
                            "sLengthMenu": "<span>_MENU_</span>"
                        },
                        "order": [],
                        "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        }],
                        aLengthMenu: [
                            [25, 50, 100, 200, 500],
                            [25, 50, 100, 200, 500]
                        ],
                        "iDisplayLength": 25,
                        "sDom": "<'row'<'col-md-6 col-xs-12 p0 'l><'col-md-6 col-xs-12 p0'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>"
                    });
                });
            }
        });
    </SCRIPT>
<?php echo str_replace(['<SCRIPT>', '</SCRIPT>'], '', ob_get_clean()); ?>
<?php $this->Html->scriptEnd(); ?>