<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="glyphicon glyphicon-ban-circle alert-icon "></i>
    <!--strong>Oh snap!</strong--><?php echo h($message) ?>
</div>