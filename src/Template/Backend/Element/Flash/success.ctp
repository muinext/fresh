<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-check alert-icon "></i>
    <strong>Well done!</strong> <?php echo h($message) ?>
</div>