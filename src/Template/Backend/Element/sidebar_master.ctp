<!--Sidebar background-->
<div id="sidebarbg" class="hidden-lg hidden-md hidden-sm hidden-xs"></div>
<!--Sidebar content-->
<div id="sidebar" class="page-sidebar hidden-lg hidden-md hidden-sm hidden-xs">
    <!-- End search -->
    <!-- Start .sidebar-inner -->
    <div class="sidebar-inner">
        <!-- Start .sidebar-scrollarea -->
        <div class="sidebar-scrollarea">
            <div class="sidenav">
                <div class="sidebar-widget mb0">
                    <h6 class="title mb0">Navigation</h6>
                </div>
                <!-- End .sidenav-widget -->
                <div class="mainnav">
                    <ul>
                        <li>
                            <a class="Users" href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'dashboard', 'plugin' => false]); ?>"><i class="s24 icomoon-icon-home-2"></i><span class="txt">Home</span> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End sidenav -->
        </div>
        <!-- End .sidebar-scrollarea -->
    </div>
    <!-- End .sidebar-inner -->
</div>
<!-- End #sidebar -->
<!--Sidebar background-->

<?php echo $this->element('snippets/sidebar_active_nav'); ?>