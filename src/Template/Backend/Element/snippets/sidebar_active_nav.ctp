<!-- Anchor class format: Controller_action -->
<?php $this->Html->scriptStart(['block' => true]); ?>
<?php ob_start(); ?>
<SCRIPT>
    var activeController = '<?php echo $this->request->params['controller']; ?>';
    var activeRequest = '<?php echo $this->request->params['controller'] . '_' . $this->request->params['action']; ?>';
    $('div.mainnav > ul > li > a').each(function () {
        if ($(this).hasClass(activeController)) {
            $(this).addClass('active');
        }
    });
    $('div.mainnav > ul > li > ul > li > a').each(function () {
        if ($(this).hasClass(activeController)) {
            $(this).addClass('active');
            $(this).parent('li').parent('ul').css({'display': 'block'});
            $(this).parent('li').parent('ul').addClass('show');
        }
    });
</SCRIPT>
<?php echo str_replace(['<SCRIPT>', '</SCRIPT>'], '', ob_get_clean()); ?>
<?php $this->Html->scriptEnd(); ?>