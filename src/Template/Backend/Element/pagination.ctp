<div class="paginator">
    <ul class="pagination">
        <?php echo $this->Paginator->prev('< ' . __('previous')) ?>
        <?php echo $this->Paginator->numbers() ?>
        <?php echo $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?php echo $this->Paginator->counter() ?></p>
</div>