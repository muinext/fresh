<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('Edit Group'), ['action' => 'edit', $group->id]) ?> </li>
        <li role="presentation"><?php echo $this->Form->postLink(__('Delete Group'), ['action' => 'delete', $group->id], ['confirm' => __('Are you sure you want to delete # {0}?', $group->id)]) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('List Groups'), ['action' => 'index']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('New Group'), ['action' => 'add']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo h($group->name) ?></strong></h1>
        </div>
        <!-- /tile header -->
        <div class="tile-body">
            <table class="table table-custom dt-responsive" id="responsive-usage" width="100%">
                <tr>
                    <th><?php echo __('Name') ?></th>
                    <td><?php echo h($group->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?php echo h($group->created) ?></tr>
                </tr>
            </table>
        </div>
    </section>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h3 class="custom-font"><strong><?php echo __('Related Users') ?></strong></h3>
        </div>
        <!-- /tile header -->
        <!-- tile body -->
        <div class="tile-body">
            <?php if (!empty($group->users)): ?>
                <table class="table table-custom dt-responsive" id="responsive-usage" width="100%">
                    <tr>
                        <th>SL</th>
                        <th><?php echo __('Email') ?></th>
                        <th><?php echo __('First Name') ?></th>
                        <th><?php echo __('Last Name') ?></th>
                        <th><?php echo __('Created') ?></th>
                        <th class="actions"><?php echo __('Actions') ?></th>
                    </tr>
                    <?php foreach ($group->users as $key => $users): ?>
                        <tr>
                            <td><?php echo($key + 1); ?></td>
                            <td><?php echo h($users->email) ?></td>
                            <td><?php echo h($users->first_name) ?></td>
                            <td><?php echo h($users->last_name) ?></td>
                            <td><?php echo h($users->created) ?></td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                                <?php echo $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                                <?php echo $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
        <!-- /tile body -->
    </section>
</div>
