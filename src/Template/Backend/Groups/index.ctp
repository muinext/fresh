<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('New Group'), ['action' => 'add']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo __('Groups') ?></strong></h1>
        </div>
        <!-- /tile header -->
        <!-- tile body -->
        <div class="tile-body">
            <table class="table table-bordered table-striped table-hover table-custom dt-responsive" id="responsive-datatables" width="100%">
                <thead>
                <tr>
                    <th>SL</th>
                    <th><?php echo $this->Paginator->sort('name') ?></th>
                    <th><?php echo $this->Paginator->sort('created') ?></th>
                    <th><?php echo $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?php echo __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($groups as $key => $group): ?>
                    <tr>
                        <td><?php echo ($key+1); ?></td>
                        <td><?php echo h($group->name) ?></td>
                        <td><?php echo h($group->created) ?></td>
                        <td><?php echo h($group->modified) ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('View'), ['action' => 'view', $group->id]) ?>
                            <?php echo $this->Html->link(__('Edit'), ['action' => 'edit', $group->id]) ?>
                            <?php echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->id], ['confirm' => __('Are you sure you want to delete # {0}?', $group->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /tile body -->
    </section>
</div>
