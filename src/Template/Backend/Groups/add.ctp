<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('List Groups'), ['action' => 'index']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?php echo $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo __('Add Group') ?></strong></h1>
        </div>
        <!-- /tile header -->
        <!-- tile body -->
        <div class="tile-body">

            <?php echo $this->Form->create($group, ['role' => 'form', 'novalidate' => 'novalidate']) ?>

            <div class="form-group">
                <?php
                echo $this->Form->input('name', ['class' => 'form-control', 'placeholder' => 'Enter Name']);
                ?>
            </div>
            <div class="form-group">
                <?php
                ?>
            </div>
            <div class="form-group">
                <?php
                ?>
            </div>
            <?php echo $this->Form->button(__('Submit'), ['class' => 'btn btn-rounded btn-success btn-sm']) ?>
            <?php echo $this->Form->end() ?>

        </div>
        <!-- /tile body -->

    </section>
</div>
