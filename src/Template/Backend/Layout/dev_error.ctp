<!doctype html>
<!--[if lt IE 8]>
<html class="no-js lt-ie8"> <![endif]-->
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Error: <?= h($this->fetch('title')) ?></title>
    <!-- Mobile specific metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Force IE9 to render in normal mode -->
    <!--[if IE]>
    <meta http-equiv="x-ua-compatible" content="IE=9"/><![endif]-->
    <meta name="author" content=""/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="application-name" content=""/>
    <!-- Import google fonts - Heading first/ text second -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" type="text/css">
    <!-- Css files -->
    <!-- Icons -->
    <link href="<?php echo $this->Url->build('/'); ?>supr/css/icons.css" rel="stylesheet"/>
    <!-- Bootstrap stylesheets (included template modifications) -->
    <link href="<?php echo $this->Url->build('/'); ?>bowers/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Plugins stylesheets (all plugin custom css) -->
    <link href="<?php echo $this->Url->build('/'); ?>supr/css/plugins.css" rel="stylesheet"/>
    <!-- Main stylesheets (template main css file) -->
    <link href="<?php echo $this->Url->build('/'); ?>supr/css/main.css" rel="stylesheet"/>

    <?php echo $this->fetch('styleBlock'); ?>

    <!-- Custom stylesheets ( Put your own changes here ) -->
    <link href="<?php echo $this->Url->build('/'); ?>supr/css/custom.css" rel="stylesheet"/>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->Url->build('/'); ?>supr/img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->Url->build('/'); ?>supr/img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->Url->build('/'); ?>supr/img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $this->Url->build('/'); ?>supr/img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="icon" href="<?php echo $this->Url->build('/'); ?>supr/img/ico/favicon.ico" type="image/png">
    <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
    <meta name="msapplication-TileColor" content="#3399cc"/>
    <style>
        body {
            font: 14px helvetica, arial, sans-serif;
            color: #222;
            background-color: #f8f8f8;
            padding: 0;
            margin: 0;
            max-height: 100%;
        }

        .code-dump,
        pre {
            background: #fefefe;
            border: 1px solid #ddd;
            padding: 5px;
            white-space: pre-wrap;
        }

        header {
            background-color: #C3232D;
            color: #ffffff;
            padding: 16px 10px;
            border-bottom: 3px solid #626262;
        }

        .header-title {
            margin: 0;
            font-weight: normal;
            font-size: 30px;
            line-height: 64px;
        }

        .header-type {
            opacity: 0.75;
            display: block;
            font-size: 16px;
            line-height: 1;
        }

        .header-help {
            font-size: 12px;
            line-height: 1;
            position: absolute;
            top: 30px;
            right: 16px;
        }

        .header-help a {
            color: #fff;
        }

        .error-nav {
            float: left;
            width: 30%;
        }

        .error,
        .error-subheading {
            font-size: 18px;
            margin-top: 0;
            padding: 10px;
            border: 1px solid #EDBD26;
        }

        .error-subheading {
            background: #1798A5;
            color: #fff;
            border: 1px solid #02808C;
        }

        .error {
            background: #ffd54f;
        }

        .customize {
            opacity: 0.6;
        }

        .stack-trace {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .stack-frame {
            padding: 10px;
            border-bottom: 1px solid #e5e5e5;
        }

        .stack-frame:last-child {
            border-bottom: none;
        }

        .stack-frame a {
            display: block;
            color: #212121;
            text-decoration: none;
        }

        .stack-frame.active {
            background: #e5e5e5;
        }

        .stack-frame a:hover {
            text-decoration: underline;
        }

        .stack-file,
        .stack-function {
            display: block;
            margin-bottom: 5px;
        }

        .stack-frame-file,
        .stack-file {
            font-family: consolas, monospace;
        }

        .stack-function {
            font-weight: bold;
        }

        .stack-file {
            font-size: 0.9em;
            word-wrap: break-word;
        }

        .stack-details {
            background: #ececec;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            border: 1px solid #ababab;
            padding: 10px;
            margin-bottom: 18px;
        }

        .stack-frame-args {
            float: right;
        }

        .toggle-link {
            color: #1798A5;
            text-decoration: none;
            text-transform: capitalize;
        }

        .toggle-link:hover {
            text-decoration: underline;
        }

        .toggle-vendor-frames {
            padding: 5px 0px;
            display: block;
        }

        .code-excerpt {
            width: 100%;
            margin: 5px 0;
            background: #fefefe;
        }

        .code-highlight {
            display: block;
            background: #fff59d;
        }

        .excerpt-line {
            padding-left: 2px;
        }

        .excerpt-number {
            background: #f6f6f6;
            width: 50px;
            text-align: right;
            color: #666;
            border-right: 1px solid #ddd;
            padding: 2px;
        }

        .excerpt-number:after {
            content: attr(data-number);
        }

        table {
            text-align: left;
        }

        th, td {
            padding: 4px;
        }

        th {
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- .#header -->
<div id="header">
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                Fresh<span class="slogan">Demo</span>
            </a>
        </div>
        <div id="navbar-no-collapse" class="navbar-no-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <!--Sidebar collapse button-->
                    <a href="#" class="collapseBtn leftbar"><i class="s16 minia-icon-list-3"></i></a>
                </li>
            </ul>
            <ul class="nav navbar-right usernav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
                        <img src="<?php echo $this->Url->build('/'); ?>supr/img/avatar.jpg" alt="" class="image"/>
                        <span class="txt"><?php echo @$authUser['email']; ?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu right">
                        <li class="menu">
                            <ul>
                                <li><a href="#"><i class="s16 icomoon-icon-user-plus"></i>Edit profile</a></li>
                                <li><a href="#"><i class="s16 icomoon-icon-bubble-2"></i>System Settings</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'logout', 'plugin' => false, 'prefix' => false]) ?>"><i
                            class="s16 icomoon-icon-exit"></i><span class="txt"> Logout</span></a></li>
            </ul>
        </div>
        <!-- /.nav-collapse -->
    </nav>
    <!-- /navbar -->
</div>
<!-- / #header -->
<div id="wrapper">
    <!-- #wrapper -->
    <?php echo $this->element('sidebar_master'); ?>
    <!--Body content-->
    <div id="content" class="page-content clearfix">
        <div class="contentwrapper pt5">
            <!--Content wrapper-->
            <!-- End  / heading-->
            <!-- Start .row -->
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend><?= h($this->fetch('title')) ?></legend>

                        <div class="error-contents">
                            <?php if ($this->fetch('subheading')): ?>
                                <p class="error-subheading">
                                    <?= $this->fetch('subheading') ?>
                                </p>
                            <?php endif; ?>

                            <?= $this->element('exception_stack_trace'); ?>

                            <div class="error-suggestion">
                                <?= $this->fetch('file') ?>
                            </div>

                            <?php if ($this->fetch('templateName')): ?>
                                <p class="customize">
                                    If you want to customize this error message, create
                                    <em><?= APP_DIR . DIRECTORY_SEPARATOR . 'Template' . DIRECTORY_SEPARATOR . 'Error' . DIRECTORY_SEPARATOR . $this->fetch('templateName') ?></em>
                                </p>
                            <?php endif; ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Vendor Stack Trails</legend>
                        <?= $this->element('exception_stack_trace_nav') ?>
                    </fieldset>

                    <script type="text/javascript">
                        function bindEvent(selector, eventName, listener) {
                            var els = document.querySelectorAll(selector);
                            for (var i = 0, len = els.length; i < len; i++) {
                                els[i].addEventListener(eventName, listener, false);
                            }
                        }

                        function toggleElement(el) {
                            if (el.style.display === 'none') {
                                el.style.display = 'block';
                            } else {
                                el.style.display = 'none';
                            }
                        }

                        function each(els, cb) {
                            var i, len;
                            for (i = 0, len = els.length; i < len; i++) {
                                cb(els[i], i);
                            }
                        }

                        window.addEventListener('load', function () {
                            bindEvent('.stack-frame-args', 'click', function (event) {
                                var target = this.dataset['target'];
                                var el = document.getElementById(target);
                                toggleElement(el);
                                event.preventDefault();
                            });

                            var details = document.querySelectorAll('.stack-details');
                            var frames = document.querySelectorAll('.stack-frame');
                            bindEvent('.stack-frame a', 'click', function (event) {
                                each(frames, function (el) {
                                    el.classList.remove('active');
                                });
                                this.parentNode.classList.add('active');

                                each(details, function (el) {
                                    el.style.display = 'none';
                                });

                                var target = document.getElementById(this.dataset['target']);
                                toggleElement(target);
                                event.preventDefault();
                            });

                            bindEvent('.toggle-vendor-frames', 'click', function (event) {
                                each(frames, function (el) {
                                    if (el.classList.contains('vendor-frame')) {
                                        toggleElement(el);
                                    }
                                });
                                event.preventDefault();
                            });
                        });
                    </script>
                </div>
            </div>
            <!-- End .row -->
        </div>
        <!-- End contentwrapper -->
    </div>
    <!-- End #content -->
    <div id="footer" class="clearfix sidebar-page right-sidebar-page">
        <!-- Start #footer  -->
        <p class="pull-left">
            Copyrights &copy; 2015 <a href="#" class="color-blue strong" target="_blank"></a>. All rights reserved.
        </p>
    </div>
    <!-- End #footer  -->
</div>
<!-- / #wrapper -->
<!-- Back to top -->
<div id="back-to-top"><a href="#">Back to Top</a>
</div>
<!-- Javascripts -->
<!-- Load pace first -->
<script data-pace-options='{ "ajax": false }' src="<?php echo $this->Url->build('/'); ?>bowers/PACE/pace.min.js"></script>
<!-- Important javascript libs(put in all pages) -->
<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo $this->Url->build('/'); ?>bowers/jquery/dist/jquery.min.js">\x3C/script>')
</script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    window.jQuery || document.write('<script src="bowers/jquery-ui/jquery-ui.min.js">\x3C/script>')
</script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="bowers/jquery-migrate/jquery-migrate.min.js">\x3C/script>')
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo $this->Url->build('/'); ?>bowers/ExplorerCanvas/excanvas.js"></script>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="<?php echo $this->Url->build('/'); ?>bowers/respond/dest/respond.min.js"></script>
<![endif]-->
<!-- Bootstrap plugins -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Core plugins ( not remove ) -->
<script src="<?php echo $this->Url->build('/'); ?>supr/js/libs/modernizr.custom.js"></script>
<!-- Handle responsive view functions -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/jrespond/js/jRespond.min.js"></script>
<!-- Custom scroll for sidebars,tables and etc. -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $this->Url->build('/'); ?>supr/plugins/core/slimscroll/jquery.slimscroll.horizontal.min.js"></script>
<!-- Remove click delay in touch -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/fastclick/lib/fastclick.js"></script>
<!-- Increase jquery animation speed -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/velocity/velocity.min.js"></script>
<!-- Quick search plugin (fast search for many widgets) -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/jquery.quicksearch/dist/jquery.quicksearch.min.js"></script>
<!-- Bootbox fast bootstrap modals -->
<script src="<?php echo $this->Url->build('/'); ?>bowers/bootbox.js/bootbox.js"></script>
<!-- Other plugins ( load only nessesary plugins for every page) -->
<script src="<?php echo $this->Url->build('/'); ?>supr/plugins/forms/checkall/jquery.checkAll.js"></script>
<script src="<?php echo $this->Url->build('/'); ?>supr/js/jquery.supr.js"></script>
<script src="<?php echo $this->Url->build('/'); ?>supr/js/main.js"></script>
<script src="<?php echo $this->Url->build('/'); ?>supr/js/pages/blank.js"></script>

<?php echo $this->fetch('scriptBlock'); ?>

<?php echo $this->fetch('script'); ?>
</body>
</html>