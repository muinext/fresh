<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
            ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?php echo $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo __('Edit User') ?></strong></h1>
        </div>
        <!-- /tile header -->
        <!-- tile body -->
        <div class="tile-body">

            <?php echo $this->Form->create($user, ['role' => 'form', 'novalidate' => 'novalidate']) ?>

            <div class="form-group">
                <?php
                echo $this->Form->input('group_id', ['options' => $groups, 'class' => 'form-control', 'placeholder' => 'Enter Group Id']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Enter Email']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('password', ['value' => '', 'class' => 'form-control', 'placeholder' => 'Enter Password']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('first_name', ['class' => 'form-control', 'placeholder' => 'Enter First Name']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('last_name', ['class' => 'form-control', 'placeholder' => 'Enter Last Name']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('dob', ['empty' => true, 'default' => '', 'class' => 'form-control', 'placeholder' => 'Enter Dob']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('address', ['class' => 'form-control', 'placeholder' => 'Enter Address']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('city', ['class' => 'form-control', 'placeholder' => 'Enter City']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('state', ['class' => 'form-control', 'placeholder' => 'Enter State']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('zip', ['class' => 'form-control', 'placeholder' => 'Enter Zip']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('phone', ['class' => 'form-control', 'placeholder' => 'Enter Phone']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('website', ['class' => 'form-control', 'placeholder' => 'Enter Website']);
                ?>
            </div>
            <?php echo $this->Form->button(__('Submit'), ['class' => 'btn btn-rounded btn-success btn-sm']) ?>
            <?php echo $this->Form->end() ?>

        </div>
        <!-- /tile body -->

    </section>
</div>
