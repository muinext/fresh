<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li role="presentation"><?php echo $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?> </li>
        <li role="presentation"><?php echo $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?> </li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo h($user->name) ?></strong></h1>
        </div>
        <!-- /tile header -->
        <div class="tile-body">
            <table class="table table-custom dt-responsive" id="responsive-usage" width="100%">
                <tr>
                    <th><?php echo __('Group') ?></th>
                    <td><?php echo $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Email') ?></th>
                    <td><?php echo h($user->email) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('First Name') ?></th>
                    <td><?php echo h($user->first_name) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Last Name') ?></th>
                    <td><?php echo h($user->last_name) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Address') ?></th>
                    <td><?php echo h($user->address) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('City') ?></th>
                    <td><?php echo h($user->city) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('State') ?></th>
                    <td><?php echo h($user->state) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Zip') ?></th>
                    <td><?php echo h($user->zip) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Phone') ?></th>
                    <td><?php echo h($user->phone) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Photo') ?></th>
                    <td><?php echo h($user->photo) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Website') ?></th>
                    <td><?php echo h($user->website) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dob') ?></th>
                    <td><?php echo h($user->dob) ?></tr>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?php echo h($user->created) ?></tr>
                </tr>
            </table>
        </div>
    </section>
</div>
