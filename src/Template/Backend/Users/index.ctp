<div class="col-md-12">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><?php echo $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li role="presentation"><?php echo $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
    </ul>
    <section class="tile">
        <!-- tile header -->
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong><?php echo __('Users') ?></strong></h1>
        </div>
        <!-- /tile header -->
        <!-- tile body -->
        <div class="tile-body">
            <table class="table table-bordered table-striped table-hover table-custom dt-responsive" id="responsive-datatables" width="100%">
                <thead>
                <tr>
                    <th>SL</th>
                    <th><?php echo $this->Paginator->sort('group_id') ?></th>
                    <th><?php echo $this->Paginator->sort('email') ?></th>
                    <th><?php echo $this->Paginator->sort('first_name') ?></th>
                    <th><?php echo $this->Paginator->sort('last_name') ?></th>
                    <th class="actions"><?php echo __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $key => $user): ?>
                    <tr>
                        <td><?php echo($key + 1); ?></td>
                        <td><?php echo $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
                        <td><?php echo h($user->email) ?></td>
                        <td><?php echo h($user->first_name) ?></td>
                        <td><?php echo h($user->last_name) ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                            <?php echo $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                            <?php echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /tile body -->
    </section>
</div>
